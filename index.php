<?php

require 'vendor/autoload.php';

use Classes\Router;

$router = new Router();

$router->parseUri();