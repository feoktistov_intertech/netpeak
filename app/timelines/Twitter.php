<?php

namespace Timelines;

use Classes\TimelineInterface;

use View\View;

class Twitter implements TimelineInterface
{
    protected $view_name;

    protected $title;

    private $settings = [];

    protected $url;

    protected $requestMethod;

    protected $getfields;

    protected $twitter;


    const ACCESS_TOKEN = '908336799164006402-5WP9iTeYdsVTaDICvXcb6s67cjDTYsE';

    const ACCESS_TOKEN_SECRET = 'MmXeiAH2yYrm1q9JKj11utn6CtSr69ohAlWv96YwTXEkt';

    const CONSUMER_KEY = 'kjf8pLtsxDs6fHkgaNs90YmAy';

    const CONSUMER_SECRET = 'C9vHiVGx9cI15nimf6KT3V15JnqZ5E4PdqOheREl1Y4VsMixFy';

    const SCREEN_NAME = 'roman_netpeak';

    const COUNT = 25;


    public function __construct()
    {
        $this->view_name = 'twitter';

        $this->settings = [
            'oauth_access_token' => self::ACCESS_TOKEN,
            'oauth_access_token_secret' => self::ACCESS_TOKEN_SECRET,
            'consumer_key' => self::CONSUMER_KEY,
            'consumer_secret' => self::CONSUMER_SECRET
        ];

        $this->url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

        $this->requestMethod  = 'GET';

        $this->getfields = '?screen_name='.self::SCREEN_NAME.'&count='.self::COUNT;

        $this->twitter = new \TwitterAPIExchange($this->settings);
    }

    public function run()
    {
        $twits = $this->getTwits();

        if (empty($twits[0])) { echo 'Wait, please! ' . $twits['errors'][0]['message']; exit(); }

        $data = [
            'title' => !empty($twits[0]['user']) ? '@' . $twits[0]['user']['screen_name'] : '',
            'data' => $twits,
            'last_twit' => $twits[self::COUNT-1]['id']
        ];

        $view = new View($this->view_name, $data);

        return $view;
    }

    public function handle()
    {
        $last_twit = $_REQUEST['last_twit'];

        $twits = $this->getTwits();

        if ($twits[self::COUNT-1]['id'] != $last_twit) {
            echo json_encode($twits); exit();
        }

        echo 'false';
    }

    private function getTwits()
    {
        $twits = json_decode(
            $this->twitter->setGetfield($this->getfields)
                ->buildOauth($this->url, $this->requestMethod)
                ->performRequest(), true
        );

        return $twits;
    }
}