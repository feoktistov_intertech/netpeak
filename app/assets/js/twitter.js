function time(date) {
    var now = new Date();
    var then = new Date(date);

    var ago = now - then;

    var second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24,
        week = day * 7;

    if (isNaN(ago) || ago < 0) return "";

    if (ago < second * 2) return "right now";

    if (ago < minute) return Math.floor(ago / second) + " seconds ago";

    if (ago < minute * 2) return "about 1 minute ago";

    if (ago < hour) return Math.floor(ago / minute) + " minutes ago";

    if (ago < hour * 2) return "about 1 hour ago";

    if (ago < day) return  Math.floor(ago / hour) + " hours ago";

    if (ago > day && ago < day * 2) return "yesterday";

    if (ago < day * 365) return Math.floor(ago / day) + " days ago";

    return "over a year ago";
}

function appendTime() {
    $('[time]').each(function (key, item) {
        var date = $(item).data('created_at');
        var ago = time(date);
        $(this).html(ago)
    });
}