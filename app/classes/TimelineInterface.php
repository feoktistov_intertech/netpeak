<?php


namespace Classes;


interface TimelineInterface
{
    public function __construct();

    public function run();

    public function handle();
}