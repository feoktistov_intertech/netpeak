<?php

namespace Classes;


class Router
{
    public $controller = '';

    public $action = '';

    public $args = [];

    function parseUri()
    {
        $basepath = implode('/', array_slice(
            explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';

        $trim = substr($_SERVER['REQUEST_URI'], strlen($basepath));

        if (!empty($trim)) {

            $parse_uri = parse_url($trim);

            if (!empty($parse_uri['path'])) {
                $path = $parse_uri['path'];

                $uri =  explode('/', $path);

                $this->controller = !empty($uri[0]) ? $uri[0] : '';

                $this->action = !empty($uri[1]) ? $uri[1] : '';

                $args = !empty($parse_uri['query']) ? explode('&', $parse_uri['query']) : [];

                if (!empty($args)) {
                    foreach ($args as $param) {
                        $arg = explode('=', $param);
                        $this->args[$arg[0]] = !empty($arg[1]) ? $arg[1] : '';
                    }
                }
            }
        }

        $this->run();
    }

    function run()
    {
        $controllerName = '\\Controllers\FeedController';

        if (!empty($this->controller) && !empty($this->action))  {

            $controllerName = '\\Controllers\\'.ucfirst($this->controller).'Controller';

            try {
                $controller = new $controllerName();
            } catch (\Error $exception) {
                echo $exception->getMessage(); exit;
            }

            $action = $this->action;

            try {
                $controller->$action($this->args);
            } catch (\Error $exception) {
                echo $exception->getMessage(); exit;
            }

        } else {
            $controller = new $controllerName();

            $controller->index();
        }

    }
}