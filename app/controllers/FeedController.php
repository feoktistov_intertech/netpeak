<?php

namespace Controllers;

use Timelines\Twitter;

class FeedController
{
    public function index()
    {
        $twitter = new Twitter();

        $view = $twitter->run();

        echo $view->render();
    }

    public function update()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $twitter = new Twitter();

            $twitter->handle();
        }
    }
}