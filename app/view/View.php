<?php

namespace View;


class View
{
    protected $viewPath = __DIR__.'/';

    protected $view;

    protected $data;

    public function __construct($view, $data =[])
    {
        $this->view = $view;

        $this->data = $data;
    }

    public function render()
    {
        if(file_exists($this->viewPath.$this->view.'.php')) {

            extract($this->data);

            ob_start();

            include $this->viewPath.$this->view.'.php';

            $buffer = ob_get_contents();

            @ob_end_clean();

            return $buffer;

        } else {
            throw new \Exception('Template not found!');
        }
    }
}