<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="/vendor/components/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/assets/css/twitter.css">
</head>
<body>
<div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <br><br>
        <h1><b><?= $title ?></b></h1><br>
        <div id="timeline">
            <?php foreach ($data as $k => $twit): ?>

                <div class="tweet my-tweet">
                    <div class="content">
                        <div class="stream-item-header">
                            <a class="account-group">
                                <img class="avatar" src="<?= $twit['user']['profile_image_url_https'] ?>">
                                <span>
                                    <strong class="fullname show-popup-with-id"><?= $twit['user']['name'] ?> </strong>
                                    <span></span>
                                    <span>&nbsp;</span>
                                </span>
                                <span class="username u-dir">@<b><?= $twit['user']['screen_name'] ?></b></span></a>
                            <small class="time">
                                <a><span time data-created_at="<?= $twit['created_at'] ?>"><?= $twit['created_at'] ?></span></a>
                            </small>
                        </div>
                        <div><p class="tweet-text">
                                <a class="twitter-atreply pretty-link"><s>@</s><b><?= $twit['user']['screen_name'] ?></b></a> <?= $twit['text'] ?>
                            </p></div>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>
    </div>
    <div class="col-xs-2"></div>
</div>

<script src="/vendor/components/jquery/jquery.js"></script>
<script src="/app/assets/js/twitter.js"></script>
<script>
    (function () {

        var handle = function () {

            appendTime();

            $.ajax({
                url: '/feed/update',
                method: 'POST',
                data: {
                    last_twit: <?= $last_twit ?>
                },
                success: function (response) {

                    if (response === 'false') {
                        handle();
                    } else {
                        var timeline = $('#timeline');
                        timeline.html('');

                        try {

                        var json = JSON.parse(response);
                        $(json).each(function (key, value) {
                            timeline.append(
                                '<div class="tweet my-tweet">' +
                                '<div class="content">' +
                                '<div class="stream-item-header">' +
                                '<a class="account-group">' +
                                '<img class="avatar" src="' + value.user.profile_image_url_https + '">' +
                                '<span>' +
                                '<strong class="fullname show-popup-with-id">' + value.user.name + ' </strong>' +
                                '<span></span>' +
                                '<span>&nbsp;</span>' +
                                '</span>' +
                                '<span class="username u-dir">@<b>' + value.user.screen_name + '</b></span></a>' +
                                '<small class="time">' +
                                '<a><span time data-created_at="' + value.created_at + '">' + value.created_at + '</span></a>' +
                                '</small>' +
                                '</div>' +
                                '<div><p class="tweet-text"><a class="twitter-atreply pretty-link"><s>@</s><b>' + value.user.screen_name + '</b></a> ' + value.text + '</p></div>'
                            );
                        });

                        } catch(error) {
                            handle();
                        }


                        handle();
                    }
                }
            });
        };
        handle();
    })();
</script>
</body>
</html>
